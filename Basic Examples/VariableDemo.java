/* 
 * Basic Variables Demo in Java
 * Demonstrates int, string, boolean
 */

public class VariableDemo {
    public static void main(String args []) {
        // Variables follow camel-case naming

        // Integer
        // Declare and define either separately:
        int numberOne;
        numberOne = 100;
        // Or all at once:
        int numberTwo = 200;

        // Floating Point
        float numberThree = 300.0;

        // Double (64-bit float)
        double numberFour = 400.0;

        // Unicode Character
        char characterOne = "a";

        // Boolean
        boolean booleanOne = "true";

        // String
        // Haha, Java's strings aren't a primitive data type
        // Rather, they're instances of a "String" object, thus
        // one must understand the object to utilize String.
    }
}